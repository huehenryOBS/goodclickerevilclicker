﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class: AudioManager
// Purpose: The Audio Manager holds all of the important functions and sound clips used in the game, 
//          so they can be accessed from one central location.

public class AudioManager : MonoBehaviour {

	public static AudioManager instance; // Our singleton for our Audio Manager
	public SoundList sounds; // a list of all sounds used in the game

	// Use this for initialization
	void Awake () {
		// If the Audio Manager has not been set
		if (AudioManager.instance == null) 
		{
			// Then set this object as the Game Manager
			instance = this;

			// Make sure this object persists between scenes
			DontDestroyOnLoad(gameObject);
		}
		else 
		{
			// Else, we already have a game manager, so destroy this object.
			Debug.LogError ("ERROR: Audio Manager already exists. You should have exactly ONE in your game.");
			Destroy (gameObject);
		}	
	}
}

// Class: Game.SoundList
// Purpose: This holds a list of all sounds in the game, so our sound engineers will never have to hunt for a sound in code. 
//          They are always right here on the main game object.
[System.Serializable]
public class SoundList
{
	public AudioClip purchaseSuccess;
	public AudioClip purchaseFail;
	public AudioClip sellSuccess;
	public AudioClip sellFail;
	public AudioClip farmerSell;
}