using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// Class: TreeLocation
// Purpose: A TreeLocation is a game object (a set of sprites in the game world) where a tree can be placed.

public class TreeLocation : MonoBehaviour {

	// Public Variables
	public bool hasSign;
	public bool hasFarmer;

	public SpriteRenderer treeGraphics;
	public SpriteRenderer signGraphics;
	public SpriteRenderer farmerGraphics;

	public Sprite treeNoFruit;
	public Sprite treeWithFruit;

	public Sprite farmerNoFruit;
	public Sprite farmerWithFruit;


	// Private Variables
	private float nextFruitTime;
	private float nextFarmerSaleTime;

	// Use this for initialization
	void Start () {
		nextFruitTime = Time.time;
		nextFarmerSaleTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		// Update graphics based on what objects we have
		if ( isFruiting() )
		{
			treeGraphics.sprite = treeWithFruit;
		}
		else
		{
			treeGraphics.sprite = treeNoFruit;
		}


		// If they have a farmer, 
		if ( hasFarmer )
		{
			// If it is more than halfway to farmer autocollect, show oranges
			if (Time.time > nextFarmerSaleTime - (GameManager.instance.data.defaultFarmerSaleTime / 2))
			{
				farmerGraphics.sprite = farmerWithFruit;
			}
			else
			{
				farmerGraphics.sprite = farmerNoFruit;
			}

			// If it is time for farmer, then collect the farmer's fruit automatically
			if (Time.time > nextFarmerSaleTime)
			{
				// Play success sound (if it exists)
				if (AudioManager.instance.sounds.farmerSell != null)
				{
					AudioSource.PlayClipAtPoint(AudioManager.instance.sounds.farmerSell, Camera.main.transform.position, GameManager.instance.options.volumeFX);
				}

				// show particle (if it exists)
				if (GameManager.instance.moneyParticlePrefab != null)
				{
					Instantiate (GameManager.instance.moneyParticlePrefab, farmerGraphics.transform.position, Quaternion.identity);
				}


				// Add appropriate credits
				GameManager.instance.player.credits += FruitSalePrice();

				// Set new timer
				nextFarmerSaleTime = Time.time + GameManager.instance.data.defaultFarmerSaleTime;

			}
		}

	}

	// Function: Is Fruiting
	// Purpose: Returns true if fruit is ready to be picked, false otherwise
	public bool isFruiting()
	{
		if ( Time.time > nextFruitTime)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public void OnMouseUpAsButton() {
		OnClick ();
	}

	public void OnClick ()
	{
		// If we are fruiting:
		if ( isFruiting() )
		{
			// Play success sound (if it exists)
			if (AudioManager.instance.sounds.sellSuccess != null)
			{
				AudioSource.PlayClipAtPoint(AudioManager.instance.sounds.sellSuccess, Camera.main.transform.position, GameManager.instance.options.volumeFX);
			}

			// show particle (if it exists)
			if (GameManager.instance.moneyParticlePrefab != null)
			{
				Instantiate (GameManager.instance.moneyParticlePrefab, treeGraphics.transform.position, Quaternion.identity);
			}

			// Add appropriate credits
			GameManager.instance.player.credits += FruitSalePrice();

			// Calculate the next time we should fruit
			// Note that this change in time will automatically make it so we are no longer fruiting.
			nextFruitTime = Time.time + FruitDelay();
		}
		else
		{
			// Otherwise, play the fail sound (if it exists)
			if (AudioManager.instance.sounds.sellFail != null)
			{
				AudioSource.PlayClipAtPoint(AudioManager.instance.sounds.sellFail, Camera.main.transform.position, GameManager.instance.options.volumeFX);
			}

		}
	}

	// Function: FruitSalePrice()
	// Purpose: Returns the fruit sale price based on all modifiers and data
	// Note: As gameplay expands, additional modifiers can be added to this function!
	public float FruitSalePrice()
	{
		// Start at the default amount
		float totalPrice = GameManager.instance.data.defaultFruitSale;

		// If we have a sign, increase the price based on the modifier
		if ( hasSign )
		{
			totalPrice *= GameManager.instance.data.signModifier;
		}

		// Send back the modified price
		return totalPrice;
	}


	// Function: FruitDelay
	// Purpose: Returns the delay time before fruit spawnings
	public float FruitDelay()
	{
		// Start at the default delay
		float totalDelay = GameManager.instance.data.defaultFruitTime;

		// If we have a farmer, then reduce based on the modifier
		if ( hasFarmer  )
		{
			totalDelay *= (1 / GameManager.instance.data.farmerModifier);
		}

		// Send back the total
		return totalDelay;
	}

}
