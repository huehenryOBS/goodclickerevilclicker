﻿using UnityEngine;
using System.Collections;

// Class: Player
// Purpose: The Player Class holds all relevant player data

public class Player : MonoBehaviour {

	public float credits; // How much money they have
	public int numTrees; // How many trees they have
	public int numFarmers; // How many farmers they have
	public int numSigns; // How many signs they have
}
