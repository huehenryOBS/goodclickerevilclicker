using UnityEngine;
using System.Collections;

// Class: ParticleSystemSelfDestruct
// Purpose: This component, when attached to an object with a particle system, will delete that system after a given time,
//          or, if an invalid time (-1) is given, it will destroy the particles after one duration loop.

public class ParticleSystemSelfDestruct : MonoBehaviour {

	public float destroyTime = -1;
	public ParticleSystem ps;

	// Use this for initialization
	void Start () {
		ps = GetComponent<ParticleSystem> ();

		if (destroyTime < 0)
			destroyTime = ps.main.duration;

		Destroy(this.gameObject, destroyTime);
	}
}
