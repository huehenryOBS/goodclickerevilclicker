﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// Class: GameManager
// Purpose: The GameManager handles all of the game logic for the game

public class GameManager : MonoBehaviour {

	// Public Variables
	public static GameManager instance = null;

	public Player player; // The player object
	public Options options; // Our game options
	public Costs data; // The data used to balance the game

	public TreeLocation[] trees;
	public StoreButtons storeButtons;
	public GameObject moneyParticlePrefab; // Particle effect for when player gets credits
	public GameObject victoryScreen; 
	[Tooltip("To win, you must have all items and this amount of $$")] public float winValue; //To win, you must have all items and this amount of $$

	// Private Variables

	// Awake happens before all Start functions
	void Awake () {
		// If the Game Manager has not been set
		if (GameManager.instance == null) 
		{
			// Then set this object as the Game Manager
			instance = this;

			// Make sure this object persists between scenes
			DontDestroyOnLoad(gameObject);
		}
		else 
		{
			// Else, we already have a game manager, so destroy this object.
			Debug.LogError ("ERROR: Game Manager already exists. You should have exactly ONE in your game.");
			Destroy (gameObject);
		}
	}

	// Use this for initialization
	void Start () {
		// Reset the Game
		ResetGame();
	}
	
	// Update is called once per frame
	void Update () {
	
		// Make sure the store is up to date
		UpdateStore ();
		DrawScene ();
		CheckForVictory ();
		CheckForQuit ();
	}

	// Function: CheckForQuit
	// Purpose: Quits the game if the escape key is pressed
	public void CheckForQuit() {
		// If they pressed escape
		if (Input.GetKeyDown (KeyCode.Escape)) {
			// Quit the Application
			Application.Quit ();
		}
	}

	// Function: CheckForVictory
	// Purpose: Shows Victory Screen if we have all the objects and > $Win
	public void CheckForVictory() {
		// If we have less than the victory dollar amount
		if (player.credits < winValue) {
			// leave this function -- we don't need to do anything if we've not reached victory.
			return;
		}

		// Look at each tree, if even one is not full, we have not won yet.
		for (int i = 0; i < trees.Length; i++) {
			// If we don't have a farmer or a tree, we have not reached victory yet
			if (!trees [i].hasSign || !trees [i].hasFarmer) {
				// leave this function -- we don't need to do anything if we've not reached victory.
				return;
			}
		}

		// We only get this far if we have the victory number of credits and all of our trees have farmers and signs.
		// Reset the Game
		ResetGame();

		// Turn on the Victory Screen
		victoryScreen.SetActive(true);
	}

	// Function: ResetGame
	// Purpose: Resets the game 
	public void ResetGame() {
		
		// Reset all the player values
		player.numTrees = 0;
		player.numFarmers = 0;
		player.numSigns = 0;
		player.credits = 0;

		// Reset all the trees
		foreach (TreeLocation tree in trees) {
			tree.gameObject.SetActive (false);
			tree.hasFarmer = false;
			tree.hasSign = false;
		}
	}

	// Function: Show Game
	// Purpose: Turns off the victory screen and shows the game
	public void ShowGame() { 
		// Turn off the Victory Screen
		victoryScreen.SetActive(false);
	}

	// Function: UpdateStore
	// Purpose: Keeps the store credits and buttons up to date
	void UpdateStore()
	{
		// Make sure the credit count is updated.
		GameManager.instance.storeButtons.creditText.text = "Credits: $" + GameManager.instance.player.credits;

		// Update the buttons

		// TREE BUTTON
		// If we are out of trees, OR the next tree is too expensive, disable the button
		if ( GameManager.instance.player.numTrees >= GameManager.instance.trees.Length || 
		     GameManager.instance.player.credits < GameManager.instance.data.treeCosts[GameManager.instance.player.numTrees] )
		{
			GameManager.instance.storeButtons.buyTreeButton.interactable = false;
		}
		// Otherwise, activate the button
		else 
		{
			GameManager.instance.storeButtons.buyTreeButton.interactable = true;
		}

		// Set the value to the correct cost, assuming there is one
		if ( GameManager.instance.player.numTrees >= GameManager.instance.trees.Length )
		{
			GameManager.instance.storeButtons.buyTreeButtonText.text = "NOT AVAILABLE";
		}
		else
		{
			GameManager.instance.storeButtons.buyTreeButtonText.text = "$" + GameManager.instance.data.treeCosts[GameManager.instance.player.numTrees];
		}


		// FARMER BUTTON
		// If we are out of farmers OR we don't have an extra tree yet OR the next farmer is too costly, disable the button
		if ( GameManager.instance.player.numFarmers >= GameManager.instance.trees.Length ||
		     GameManager.instance.player.numFarmers >= GameManager.instance.player.numTrees ||
		     GameManager.instance.player.credits < GameManager.instance.data.farmerCosts[GameManager.instance.player.numFarmers] )
		{
			GameManager.instance.storeButtons.buyFarmerButton.interactable = false;
		}
		// Otherwise, activate the button
		else 
		{
			GameManager.instance.storeButtons.buyFarmerButton.interactable = true;
			GameManager.instance.storeButtons.buyFarmerButtonText.text = "$" + GameManager.instance.data.farmerCosts[GameManager.instance.player.numFarmers] + "";
		}

		// Set the value to the correct cost, assuming there is one
		if ( GameManager.instance.player.numFarmers >= GameManager.instance.trees.Length )
		{
			GameManager.instance.storeButtons.buyFarmerButtonText.text = "NOT AVAILABLE";
		}
		else
		{
			GameManager.instance.storeButtons.buyFarmerButtonText.text = "$" + GameManager.instance.data.farmerCosts[GameManager.instance.player.numFarmers];
		}


		// SIGN BUTTON
		// If we are out of farmers OR we don't have an extra tree yet OR the next farmer is too costly, disable the button
		if ( GameManager.instance.player.numSigns >= GameManager.instance.trees.Length ||
		    GameManager.instance.player.numSigns >= GameManager.instance.player.numTrees ||
		    GameManager.instance.player.credits < GameManager.instance.data.signCosts[GameManager.instance.player.numSigns] )
		{
			GameManager.instance.storeButtons.buySignButton.interactable = false;
		}
		// Otherwise, activate the button
		else 
		{
			GameManager.instance.storeButtons.buySignButton.interactable = true;
			GameManager.instance.storeButtons.buySignButtonText.text = "$" + GameManager.instance.data.signCosts[GameManager.instance.player.numSigns] + "";
		}

		// Set the value to the correct cost, assuming there is one
		if ( GameManager.instance.player.numSigns >= GameManager.instance.trees.Length )
		{
			GameManager.instance.storeButtons.buySignButtonText.text = "NOT AVAILABLE";
		}
		else
		{
			GameManager.instance.storeButtons.buySignButtonText.text = "$" + GameManager.instance.data.signCosts[GameManager.instance.player.numSigns];
		}

	}

	// Function: DrawScene
	// Purpose: Draws all the trees/signs/farmers/etc. based on the variables
	public void DrawScene ()
	{
		int i; 

		// Turn on/off all the trees
		for (i = 0; i < GameManager.instance.trees.Length; i++)
		{
			if ( i < GameManager.instance.player.numTrees )
			{
				GameManager.instance.trees [i].gameObject.SetActive (true); // Activate the tree location gameObject
				GameManager.instance.trees[i].treeGraphics.enabled = true; // Enable the tree graphic
			}
			else
			{
				GameManager.instance.trees[i].treeGraphics.enabled = false; // Disable the tree graphic
			}
		}

		// Turn on/off all the signs
		for (i = 0; i < GameManager.instance.trees.Length; i++)
		{
			if ( i < GameManager.instance.player.numSigns )
			{
				GameManager.instance.trees[i].signGraphics.enabled = true; // Enable the sign graphic
			}
			else
			{
				GameManager.instance.trees[i].signGraphics.enabled = false; // Disable the sign graphic
			}
		}

		// Turn on/off all the farmers
		for (i = 0; i < GameManager.instance.trees.Length; i++)
		{
			if ( i < GameManager.instance.player.numFarmers )
			{
				GameManager.instance.trees[i].farmerGraphics.enabled = true; // Enable the farmer graphic
			}
			else
			{
				GameManager.instance.trees[i].farmerGraphics.enabled = false; // Disable the farmer graphic
			}
		}
	}



	// Function: OnPurchaseTree
	// Purpose: Occurs when the player clicks the button to purchase a tree.
	public void OnPurchaseTree ( )
	{
		// Play success sound (if it exists)
		if (AudioManager.instance.sounds.purchaseSuccess != null)
		{
			AudioSource.PlayClipAtPoint(AudioManager.instance.sounds.purchaseSuccess, Camera.main.transform.position, GameManager.instance.options.volumeFX);
		}

		// Subtract credits from their account
		GameManager.instance.player.credits -= GameManager.instance.data.treeCosts[GameManager.instance.player.numTrees];

		// Add a tree to their number of trees
		GameManager.instance.player.numTrees += 1;

		// Redraw the scene
		DrawScene();
	}

	// Function: OnPurchaseSign
	// Purpose: Occurs when the player clicks the button to purchase a sign.
	// Notes: This function must run checks for itself. No checks are run before calling this function.
	public void OnPurchaseSign ( )
	{
		// Play success sound (if it exists)
		if (AudioManager.instance.sounds.purchaseSuccess != null)
		{
			AudioSource.PlayClipAtPoint(AudioManager.instance.sounds.purchaseSuccess, Camera.main.transform.position, GameManager.instance.options.volumeFX);
		}

		// Subtract credits from their account
		GameManager.instance.player.credits -= GameManager.instance.data.signCosts[GameManager.instance.player.numSigns];

		// Set the sign flag
		GameManager.instance.trees [GameManager.instance.player.numSigns].hasSign = true;

		// Add a sign
		GameManager.instance.player.numSigns += 1;
		
		// Redraw the scene
		DrawScene();
	}

	// Function: OnPurchaseFarmer
	// Purpose: Occurs when the player clicks the button to purchase a farmer.
	// Notes: This function must run checks for itself. No checks are run before calling this function.
	public void OnPurchaseFarmer ( )
	{
		// Play success sound (if it exists)
		if (AudioManager.instance.sounds.purchaseSuccess != null)
		{
			AudioSource.PlayClipAtPoint(AudioManager.instance.sounds.purchaseSuccess, Camera.main.transform.position, GameManager.instance.options.volumeFX);
		}
		
		// Subtract credits from their account
		GameManager.instance.player.credits -= GameManager.instance.data.farmerCosts[GameManager.instance.player.numFarmers];
		
		// Set the farmer flag
		GameManager.instance.trees [GameManager.instance.player.numFarmers].hasFarmer = true;

		// Add a farmer
		GameManager.instance.player.numFarmers += 1;
		
		// Redraw the scene
		DrawScene();
	}

	// Class: Game.Options
	// Purpose: This holds any game options. Right now, just the sound volumes	
	[System.Serializable]
	public class Options
	{
		public float volumeFX = 0.5f;
		public float volumeMusic = 0.5f;
	}


	// Class: Game.Costs --
	// Purpose: This holds the data used to balance the game: specifically the costs of items we can purchase.
	// Notes: Stored in a class to allow us to expand/shrink these items in the inspector
	[System.Serializable]
	public class Costs
	{
		public int[] treeCosts; // This array holds the costs for each additional tree
		public int[] farmerCosts; // This array holds the costs for each farmer
		public int[] signCosts; // This array holds the costs for each sign
		public float defaultFarmerSaleTime = 5.0f; // This is how often farmers sell oranges.
		public float defaultFruitTime = 1.5f; // This is how long it takes (in seconds) before fruit is ready to click.
		public float defaultFruitSale = 1.0f; // This is how much a fruit click usually sells for
		public float farmerModifier = 2.0f; // This is how much faster fruit grows with a farmer. Ex: 2 means fruit grows twice as fast
		public float signModifier = 5.0f; // This is how much more fruit sells for with a sign. Ex: 2 means fruit sells for double
	}

	// Class: Game.Options
	// Purpose: This holds any the game buttons, so we can access them quickly in code	
	[System.Serializable]
	public class StoreButtons
	{
		public Button buyTreeButton;
		public Button buyFarmerButton;
		public Button buySignButton;

		public Text buyTreeButtonText;
		public Text buyFarmerButtonText;
		public Text buySignButtonText;

		public Text creditText; 
	}
}
