﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {

	// Public Variables
	public static GameManager instance = null;

	public Player actor; 
	public int[] costs; 
	public int[] powerups;
	public int[] boosts; 
	public float aSaleTime = 5.0f; 
	public float growTime = 1.5f; 
	public float fruitCost = 1.0f; 
	public AudioClip purchaseSuccess;
	public float pwrupValue = 2.0f;
	public float boostAmount = 5.0f; 
	public TreeLocation[] objArray;
	public StoreButtons storeButtons;
	public GameObject effect; 
	public GameObject vs; 
	public float winValue; 
	public AudioClip purchaseFail;
	public float volumeFX = 0.5f;
	public float volumeMusic = 0.5f;

	public void ShowGame() { 
		vs.SetActive(false);
	}

	void Start () {
		actor.objects = 0;
		actor.pwups = 0;
		actor.bsts = 0;
		actor.total = 0;
		foreach (TreeLocation tree in objArray) {
			tree.gameObject.SetActive (false);
			tree.powerup = false;
			tree.booster = false;
		}
	}
	
	void Update () {
		GameManager.instance.storeButtons.creditText.text = "Credits: $" + GameManager.instance.actor.total;
		if ( GameManager.instance.actor.objects >= GameManager.instance.objArray.Length || 
			GameManager.instance.actor.total < GameManager.instance.costs[GameManager.instance.actor.objects] ) {
			GameManager.instance.storeButtons.buyTreeButton.interactable = false;
		}
		else {
			GameManager.instance.storeButtons.buyTreeButton.interactable = true;
		}
		if ( GameManager.instance.actor.objects >= GameManager.instance.objArray.Length ) {
			GameManager.instance.storeButtons.buyTreeButtonText.text = "NOT AVAILABLE";
		}
		else {
			GameManager.instance.storeButtons.buyTreeButtonText.text = "$" + GameManager.instance.costs[GameManager.instance.actor.objects];
		}
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit ();
		}
		if ( GameManager.instance.actor.pwups >= GameManager.instance.objArray.Length ||
			GameManager.instance.actor.pwups >= GameManager.instance.actor.objects ||
			GameManager.instance.actor.total < GameManager.instance.powerups[GameManager.instance.actor.pwups] ) {
			GameManager.instance.storeButtons.buyFarmerButton.interactable = false;
		}
		else {
			GameManager.instance.storeButtons.buyFarmerButton.interactable = true;
			GameManager.instance.storeButtons.buyFarmerButtonText.text = "$" + GameManager.instance.powerups[GameManager.instance.actor.pwups] + "";
		}
		if ( GameManager.instance.actor.pwups >= GameManager.instance.objArray.Length ) {
			GameManager.instance.storeButtons.buyFarmerButtonText.text = "NOT AVAILABLE";
		}
		else {
			GameManager.instance.storeButtons.buyFarmerButtonText.text = "$" + GameManager.instance.powerups[GameManager.instance.actor.pwups];
		}
		if ( GameManager.instance.actor.bsts >= GameManager.instance.objArray.Length ||
			GameManager.instance.actor.bsts >= GameManager.instance.actor.objects ||
			GameManager.instance.actor.total < GameManager.instance.boosts[GameManager.instance.actor.bsts] )
		{
			GameManager.instance.storeButtons.buySignButton.interactable = false;
		}
		else {
			GameManager.instance.storeButtons.buySignButton.interactable = true;
			GameManager.instance.storeButtons.buySignButtonText.text = "$" + GameManager.instance.boosts[GameManager.instance.actor.bsts] + "";
		}
		if ( GameManager.instance.actor.bsts >= GameManager.instance.objArray.Length )
		{
			GameManager.instance.storeButtons.buySignButtonText.text = "NOT AVAILABLE";
		}
		else {
			GameManager.instance.storeButtons.buySignButtonText.text = "$" + GameManager.instance.boosts[GameManager.instance.actor.bsts];
		}
		DrawScene ();
		if (actor.total < winValue) {
			return;
		}

		for (int i = 0; i < objArray.Length; i++) {
			if (!objArray [i].booster || !objArray [i].powerup) {
				return;
			}
		}
		vs.SetActive(true);
	}

	void Awake () {
		if (GameManager.instance == null) 
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else 
		{
			Destroy (gameObject);
		}
	}

	public void DrawScene ()
	{
		int i; 
		for (i = 0; i < GameManager.instance.objArray.Length; i++)
		{
			if ( i < GameManager.instance.actor.objects )
			{
				GameManager.instance.objArray [i].gameObject.SetActive (true); 
				GameManager.instance.objArray[i].treeGraphics.enabled = true; 
			}
			else
			{
				GameManager.instance.objArray[i].treeGraphics.enabled = false; 
			}
		}
		for (i = 0; i < GameManager.instance.objArray.Length; i++)
		{
			if ( i < GameManager.instance.actor.bsts )
			{
				GameManager.instance.objArray[i].signGraphics.enabled = true; 
			}
			else
			{
				GameManager.instance.objArray[i].signGraphics.enabled = false; 
			}
		}
		for (i = 0; i < GameManager.instance.objArray.Length; i++)
		{
			if ( i < GameManager.instance.actor.pwups )
			{
				GameManager.instance.objArray[i].farmerGraphics.enabled = true; 
			}
			else
			{
				GameManager.instance.objArray[i].farmerGraphics.enabled = false;
			}
		}
	}

	public void OnPurchaseTree ( )
	{
		if (purchaseSuccess != null)
		{
			AudioSource.PlayClipAtPoint(purchaseSuccess, Camera.main.transform.position, GameManager.instance.volumeFX);
		}
		GameManager.instance.actor.total -= GameManager.instance.costs[GameManager.instance.actor.objects];
		GameManager.instance.actor.objects += 1;
		DrawScene();
	}

	public void OnPurchaseSign ( )
	{
		if (purchaseSuccess != null)
		{
			AudioSource.PlayClipAtPoint(purchaseSuccess, Camera.main.transform.position, GameManager.instance.volumeFX);
		}
		GameManager.instance.actor.total -= GameManager.instance.boosts[GameManager.instance.actor.bsts];
		GameManager.instance.objArray [GameManager.instance.actor.bsts].booster = true;
		GameManager.instance.actor.bsts += 1;
		DrawScene();
	}

	public void OnPurchaseFarmer ( )
	{
		if (purchaseSuccess != null)
		{
			AudioSource.PlayClipAtPoint(purchaseSuccess, Camera.main.transform.position, GameManager.instance.volumeFX);
		}
		GameManager.instance.actor.total -= GameManager.instance.powerups[GameManager.instance.actor.pwups];
		GameManager.instance.objArray [GameManager.instance.actor.pwups].powerup = true;
		GameManager.instance.actor.pwups += 1;
		DrawScene();
	}

	[System.Serializable]
	public class StoreButtons
	{
		public Button buyTreeButton;
		public Button buyFarmerButton;
		public Button buySignButton;

		public Text buyTreeButtonText;
		public Text buyFarmerButtonText;
		public Text buySignButtonText;

		public Text creditText; 
	}
}
