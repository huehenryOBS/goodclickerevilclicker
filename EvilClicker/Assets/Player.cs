﻿using UnityEngine;
using System.Collections;

// Class: Player
// Purpose: The Player Class holds all relevant player data

public class Player : MonoBehaviour {

	public float total;
	public int objects;
	public int pwups; 
	public int bsts; 
}
