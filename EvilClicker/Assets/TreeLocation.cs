using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// Class: TreeLocation
// Purpose: A TreeLocation is a game object (a set of sprites in the game world) where a tree can be placed.

public class TreeLocation : MonoBehaviour {

	public bool powerup;
	private float next;
	public SpriteRenderer treeGraphics;
	public SpriteRenderer signGraphics;
	public SpriteRenderer farmerGraphics;
	public Sprite tree0;
	public Sprite tree1;
	public bool booster;
	public Sprite farmerWithFruit;
	public AudioClip sellSuccess;
	public AudioClip sellFail;
	public AudioClip farmerSell;
	public Sprite farmerNoFruit;
	private float nextFarmerSaleTime;

	// Use this for initialization
	void Start () {
		next = Time.time;
		nextFarmerSaleTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if ( Time.time > next )
		{
			treeGraphics.sprite = tree1;
		}
		else
		{
			treeGraphics.sprite = tree0;
		}


		if ( powerup )
		{
			if (Time.time > nextFarmerSaleTime - (GameManager.instance.aSaleTime / 2))
			{
				farmerGraphics.sprite = farmerWithFruit;
			}
			else
			{
				farmerGraphics.sprite = farmerNoFruit;
			}
			if (Time.time > nextFarmerSaleTime)
			{
				if (farmerSell != null)
				{
					AudioSource.PlayClipAtPoint(farmerSell, Camera.main.transform.position, GameManager.instance.volumeFX);
				}
				if (GameManager.instance.effect != null)
				{
					Instantiate (GameManager.instance.effect, farmerGraphics.transform.position, Quaternion.identity);
				}
				if (booster) {
					GameManager.instance.actor.total += GameManager.instance.fruitCost*GameManager.instance.boostAmount;
				} else {
					GameManager.instance.actor.total += GameManager.instance.fruitCost;
				}
				nextFarmerSaleTime = Time.time + GameManager.instance.aSaleTime;
			}
		}

	}

	public void OnMouseUpAsButton() {
		OnClick ();
	}

	public void OnClick ()
	{
		if ( Time.time > next )
		{
			if (sellSuccess != null)
			{
				AudioSource.PlayClipAtPoint(sellSuccess, Camera.main.transform.position, GameManager.instance.volumeFX);
			}
			if (GameManager.instance.effect != null)
			{
				Instantiate (GameManager.instance.effect, treeGraphics.transform.position, Quaternion.identity);
			}
			if (booster) {
				GameManager.instance.actor.total += GameManager.instance.fruitCost*GameManager.instance.boostAmount;
			} else {
				GameManager.instance.actor.total += GameManager.instance.fruitCost;
			}
			if (powerup) {
				next = Time.time + GameManager.instance.growTime * (1 / GameManager.instance.pwrupValue);
			} else {
				next = Time.time + GameManager.instance.growTime;
			}
		}
		else
		{
			if (sellFail != null)
			{
				AudioSource.PlayClipAtPoint(sellFail, Camera.main.transform.position, GameManager.instance.volumeFX);
			}
		}
	}
}
