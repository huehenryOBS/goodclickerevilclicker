using UnityEngine;
using System.Collections;

public class ParticleSystemSelfDestruct : MonoBehaviour {

	public float length = -1;
	public ParticleSystem ps;

	// Use this for initialization
	void Start () {
		ps = GetComponent<ParticleSystem> ();
		if (length < 0)
			length = ps.main.duration;
		Destroy(this.gameObject, length);
	}
}
